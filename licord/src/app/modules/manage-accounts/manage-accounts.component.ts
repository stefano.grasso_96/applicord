import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

interface User {
  name: string;
  lastname: string;
  mail: string;
  phone:string,
  type: string;
  note: string;
  company: string
}
@Component({
  selector: 'app-manage-accounts',
  templateUrl: './manage-accounts.component.html',
  styleUrls: ['./manage-accounts.component.scss']
})
export class ManageAccountsComponent implements OnInit {

  dataSource: MatTableDataSource<User>;
  users: User[] = [];
  columns: string[] = ['name', 'lastname','mail', 'phone', 'type','company', 'note','actions'];
  @ViewChild(MatSort, { static: true }) sort: MatSort = new MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;

  constructor() {
    this.users = [{
      name: 'Daniel',
    lastname: 'Kristeen',
    mail: 'daniel@website.com',
    phone:'33493558319',
    type: 'Responsabile',
    note: 'string',
    company:'Connecta'
    },
    {
      name: 'Rob',
      lastname: 'Kristeen',
      mail: 'daniel@website.com',
      phone:'33493558319',
      type: 'Responsabile',
      note: 'string',
      company:'Livith'
    },
    {
      name: 'Anna',
    lastname: 'Kristeen',
    mail: 'daniel@website.com',
    phone:'33493558319',
    type: 'Responsabile',
    note: 'string',
    company:'Zhero'
    },
    {
      name: 'Jack',
    lastname: 'Kristeen',
    mail: 'daniel@website.com',
    phone:'33493558319',
    type: 'Responsabile',
    note: 'string',
    company: 'Connecta'
    },
    {
      name: 'Pino',
    lastname: 'Kristeen',
    mail: 'daniel@website.com',
    phone:'33493558319',
    type: 'Responsabile',
    note: 'string',
    company:'Zhero'
    },
    {
      name: 'Daniel',
    lastname: 'Kristeen',
    mail: 'daniel@website.com',
    phone:'33493558319',
    type: 'Responsabile',
    note: 'string',
    company:'Althen'
    }];
    this.dataSource = new MatTableDataSource(this.users);
   }

  ngOnInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(event: any){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase()
  }

}
