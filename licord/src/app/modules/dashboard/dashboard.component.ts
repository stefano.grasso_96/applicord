import { Component, OnInit } from '@angular/core';

import { DashboardService } from '../dashboard.service';
import HC_exporting from 'highcharts/modules/exporting';
declare var Highcharts: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {


  historical: any;
  bigChartBattery: any;
  forza1: any;
  Vaforza1 = true;
  forza2: any;
  Vaforza2 = true;
  forza3: any;
  Vaforza3 = true;
  batteria: any;
  temperatura: any;
  Vatemp = true;
  temperatura2: any;
  Vatemp2 = true;
  series:any;
  seriesB:any;

  constructor(private dashboardService: DashboardService) {

   }

  ngOnInit() {
    /*
    this.historical = this.dashboardService.historical();
    this.bigChartBattery = this.dashboardService.bigChartBattery();

    this.createchart(this.historical, 'historical')

    this.createchart(this.bigChartBattery, 'batteria')*/
    this.forza1 = this.dashboardService.forza1();
    this.forza2 = this.dashboardService.forza2();
    this.forza3 = this.dashboardService.forza3();
    this.batteria = this.dashboardService.batteria();
    this.temperatura = this.dashboardService.temperatura();
    this.temperatura2 = this.dashboardService.temperatura2();

    this.series=[{
      name: 'Forza1',
      data: this.forza1,
      color: '#d7c233',
      legendIndex: 1,
      visible: this.Vaforza1,
      showInNavigator: true
    },
    {
      name: 'Forza2',
      data: this.forza2,
      color: '#df9d2a',
      legendIndex: 1,
      visible: this.Vaforza2,
      showInNavigator: true
    },
    {
      name: 'Forza3',
      data: this.forza3,
      color: '#dd3c23',
      legendIndex: 1,
      visible: this.Vaforza3,
      showInNavigator: true
    },
    {
      name: 'Temperatura',
      data: this.temperatura,
      color: '#39b963',
      legendIndex: 1,
      visible: this.Vatemp,
      showInNavigator: true
    },
    {
      name: 'Temperatura2',
      data: this.temperatura2,
      color: '#319de2',
      legendIndex: 1,
      visible: this.Vatemp2,
      showInNavigator: true
    }]

    this.seriesB=[{
      name: 'Batteria',
      data: this.batteria,
      color: '#14358b',
      legendIndex: 1,
      showInNavigator: true
    }]

    this.createchart( this.series ,'historical')
    this.createchart( this.seriesB ,'batteria')

    HC_exporting(Highcharts);

    setTimeout(() => {
      window.dispatchEvent(
        new Event('resize')
      );
    }, 300);
  }


  //Common options for charts
  createchart(serie:any, a: string) {
    Highcharts.stockChart(a, {

      rangeSelector: {
        //position top default
        inputPosition: {
          align: 'left',
          x: 0,
          y: 0
        },
        buttonPosition: {
          align: 'right',
          x: 0,
          y: 0
        },

      },

      yAxis: {
        opposite: false,

        plotLines: [{
          value: 0,// Value of where the line will appear
          width: 2,

          color: 'silver'
        }]
      },

      plotOptions: {
        series: {
          pointStart: Date.UTC(2022, 0, 1),
          pointInterval: 3600 * 1000 * 24 // one hour
        },

      },

      /*series: data
      [{
        name: 'Forza1',
        data: this.forza1,
        color: 'red',
        legendIndex: 1,
        visible: this.Vaforza1,
        showInNavigator: true
      },
      {
        name: 'Forza2',
        data: this.forza2,
        color: 'orange',
        legendIndex: 1,
        visible: this.Vaforza2,
        showInNavigator: true
      },
      {
        name: 'Forza3',
        data: this.forza3,
        color: 'green',
        legendIndex: 1,
        visible: this.Vaforza3,
        showInNavigator: true
      },
      {
        name: 'Temperatura',
        data: this.temperatura,
        color: 'purple',
        legendIndex: 1,
        visible: this.Vatemp,
        showInNavigator: true
      }]
      */
      series: serie
    });

  }

  Vtemp() {
   let box = document.getElementById('temp');
    this.Vatemp = !this.Vatemp
    if(this.Vatemp){
      box!.style.backgroundColor = '#39b963';
    }
    else{
     box!.style.backgroundColor = 'grey';
    }
    this.ngOnInit()
  };

  Vtemp2() {
    let box = document.getElementById('temp2');
    this.Vatemp2 = !this.Vatemp2
    if(this.Vatemp2){
      box!.style.backgroundColor = '#319de2';
    }
    else{
     box!.style.backgroundColor = 'grey';
    }
    this.ngOnInit()
  };
  Vforza1() {
    let box = document.getElementById('forza1');
    this.Vaforza1 = !this.Vaforza1
    if(this.Vaforza1){
      box!.style.backgroundColor = '#d7c233';
    }
    else{
     box!.style.backgroundColor = 'grey';
    }
    this.ngOnInit()
  };
  Vforza2() {
    let box = document.getElementById('forza2');
    this.Vaforza2 = !this.Vaforza2
    if(this.Vaforza2){
      box!.style.backgroundColor = '#df9d2a';
    }
    else{
     box!.style.backgroundColor = 'grey';
    }
    this.ngOnInit()
  };
  Vforza3() {
    let box = document.getElementById('forza3');
    this.Vaforza3 = !this.Vaforza3
    if(this.Vaforza3){
      box!.style.backgroundColor = '#dd3c23';
    }
    else{
     box!.style.backgroundColor = 'grey';
    }
    this.ngOnInit()
  };


}


