import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignLicordComponent } from './assign-licord.component';

describe('AssignLicordComponent', () => {
  let component: AssignLicordComponent;
  let fixture: ComponentFixture<AssignLicordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssignLicordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignLicordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
