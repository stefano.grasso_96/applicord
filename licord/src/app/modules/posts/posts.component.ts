import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';


interface IPost {
  token: string;
  installatore?: string;
  cliente:string,
  cantiere?: string;
  date?: string;
}

declare var google: any;
@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit, AfterViewInit {


  map: any;
  dataSource: MatTableDataSource<IPost>;
  posts: IPost[] = [];
  columns: string[] = ['token', 'installatore','cliente', 'cantiere', 'date', 'actions'];

  @ViewChild('map') mapElement: any;
  @ViewChild(MatSort, { static: true }) sort: MatSort = new MatSort;
  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  lat = 43.66703722671484;
  lng = 10.999165784352293;
  markers = [
    { lat: 45.44073139079342, lng: 9.188024264706886, icon:{url:"/assets/lowbattery.png",scaledSize: new google.maps.Size(20, 20),origin: new google.maps.Point(0,0), // origin
    anchor: new google.maps.Point(0, 0)}},
    { lat: 43.66703722671484, lng: 10.999165784352293, icon:{url:"/assets/warning.png",scaledSize: new google.maps.Size(20, 20),origin: new google.maps.Point(0,0), // origin
    anchor: new google.maps.Point(0, 0)}},
    {lat:41.91356287521891, lng:12.511271483279282, icon:{url:"/assets/nosignal.png",scaledSize: new google.maps.Size(20, 20),origin: new google.maps.Point(0,0), // origin
    anchor: new google.maps.Point(0, 0)}},
    {lat:40.87728818742964, lng:14.28121141780276,icon:{url:"/assets/cantiere.png", scaledSize: new google.maps.Size(20, 20),origin: new google.maps.Point(0,0), // origin
    anchor: new google.maps.Point(0, 0)} }



  ];

  constructor(private router: Router) {
    this.posts = [{
      token: '1',
      installatore: 'stef',
      cliente: 'Rossi',
      cantiere: 'title',
      date: '2020-02-02 10:10:10'
    },
    {
      token: '2',
      installatore: 'stef2',
      cliente: 'Rossi',
      cantiere: 'title2',
      date: '2020-02-03 10:10:10'
    },
    {
      token: '3',
      installatore: 'stef3',
      cliente: 'Rossi',
      cantiere: 'title3',
      date: '2020-02-03 10:10:10'
    },
    {
      token: '4',
      installatore: 'stef4',
      cliente: 'Rossi',
      cantiere: 'title4',
      date: '2020-02-04 10:10:10'
    },
    {
      token: '5',
      installatore: 'stef5',
      cliente: 'Rossi',
      cantiere: 'title5',
      date: '2020-02-05 10:10:10'
    },
    {
      token: '6',
      installatore: 'stef6',
      cliente: 'Rossi',
      cantiere: 'title6',
      date: '2020-02-06 10:10:10'
    }];

    this.dataSource = new MatTableDataSource(this.posts);
  }

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  ngAfterViewInit(): void {
    const mapProperties = {
      center: new google.maps.LatLng(this.lat, this.lng),
      zoom: 5,
      //gestureHandling: "none",
      scrollwheel: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
      }
    };
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);

    const input = document.getElementById("pac-input") as HTMLInputElement;
    const searchBox = new google.maps.places.SearchBox(input);
    this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    this.markers.forEach(location => {
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(location.lat, location.lng),
        icon: location.icon,
        map: this.map,
        title: "Cantiere",
        size: new google.maps.Size(5, 5)
      });
      //marker cliccabile per finestra informazioni tasto dx////////////
      const contentString =
    '<div id="content">' +
    '<div id="siteNotice">' +
    "</div>" +
    '<h1 id="firstHeading" class="firstHeading">Uluru</h1>' +
    '<div id="bodyContent">' +
    "<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large " +
    "sandstone rock formation in the southern part of the " +
    "Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) " +
    "south west of the nearest large town, Alice Springs; 450&#160;km " +
    "(280&#160;mi) by road. Kata Tjuta and Uluru are the two major " +
    "features of the Uluru - Kata Tjuta National Park. Uluru is " +
    "sacred to the Pitjantjatjara and Yankunytjatjara, the " +
    "Aboriginal people of the area. It has many springs, waterholes, " +
    "rock caves and ancient paintings. Uluru is listed as a World " +
    "Heritage Site.</p>" +
    '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">' +
    "https://en.wikipedia.org/w/index.php?title=Uluru</a> " +
    "(last visited June 22, 2009).</p>" +
    "</div>" +
    "</div>";

      const infowindow = new google.maps.InfoWindow({
        content: contentString,
      });

      
      marker.addListener("contextmenu", () => {
        infowindow.open({
          anchor: marker,
          map:this.map,
          shouldFocus: false,
        });
        //this.router.navigate(['default/articles'])
      });
///marker per collegamento testo sx
      marker.addListener("click", () => {
        this.router.navigate(['default/articles'])
      });

    });
    // Bias the SearchBox results towards current map's viewport.
    this.map.addListener("bounds_changed", () => {
      searchBox.setBounds(this.map.getBounds() as google.maps.LatLngBounds);
    });

    let markers: google.maps.Marker[] = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener("places_changed", () => {
      const places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }
      // Clear out the old markers.
      markers.forEach((marker) => {
        marker.setMap(null);
      });
      markers = [];

      // For each place, get the icon, name and location.
      const bounds = new google.maps.LatLngBounds();

      places.forEach((place: { geometry: { location: any; viewport: any; }; icon: string; name: any; }) => {
        if (!place.geometry || !place.geometry.location) {
          console.log("Returned place contains no geometry");
          return;
        }
        const icon = {
          url: place.icon as string,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25),
        };
        // Create a marker for each place.
        markers.push(
          new google.maps.Marker({
            map: this.map,
            icon,
            title: place.name,
            position: place.geometry.location,
          })
        );

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      this.map.fitBounds(bounds);
    });



  }

 

  applyFilter(event: any) {
    console.log('event', event);
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
