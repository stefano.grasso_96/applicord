import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LicordListComponent } from './licord-list.component';

describe('LicordListComponent', () => {
  let component: LicordListComponent;
  let fixture: ComponentFixture<LicordListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LicordListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LicordListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
