import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolsAdminComponent } from './tools-admin.component';

describe('ToolsAdminComponent', () => {
  let component: ToolsAdminComponent;
  let fixture: ComponentFixture<ToolsAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ToolsAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolsAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
