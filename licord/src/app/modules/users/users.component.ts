import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

interface User {
  name: string;
  lastname: string;
  mail: string;
  phone:string,
  type: string;
  note: string;
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  dataSource: MatTableDataSource<User>;
  posts: User[] = [];
  columns: string[] = ['name', 'lastname','mail', 'phone', 'type', 'note','actions'];

  @ViewChild(MatSort, { static: true }) sort: MatSort = new MatSort;
  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;

  constructor() {
    this.posts = [{
      name: 'Daniel',
    lastname: 'Kristeen',
    mail: 'daniel@website.com',
    phone:'33493558319',
    type: 'Responsabile',
    note: 'string'
    },
    {
      name: 'Rob',
      lastname: 'Kristeen',
      mail: 'daniel@website.com',
      phone:'33493558319',
      type: 'Responsabile',
      note: 'string'
    },
    {
      name: 'Anna',
    lastname: 'Kristeen',
    mail: 'daniel@website.com',
    phone:'33493558319',
    type: 'Responsabile',
    note: 'string'
    },
    {
      name: 'Jack',
    lastname: 'Kristeen',
    mail: 'daniel@website.com',
    phone:'33493558319',
    type: 'Responsabile',
    note: 'string'
    },
    {
      name: 'Pino',
    lastname: 'Kristeen',
    mail: 'daniel@website.com',
    phone:'33493558319',
    type: 'Responsabile',
    note: 'string'
    },
    {
      name: 'Daniel',
    lastname: 'Kristeen',
    mail: 'daniel@website.com',
    phone:'33493558319',
    type: 'Responsabile',
    note: 'string'
    }];

    this.dataSource = new MatTableDataSource(this.posts);
   }

  ngOnInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(event: any){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase()
  }

}
