import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

interface Worksite {
  idWorksite: string;
  serialNumber: string;
  token: string;
  type: string;
  city:string,
  provincia: string;
  via: string;
  civico: string;
  date:string;
  
}

@Component({
  selector: 'app-manage-worksite',
  templateUrl: './manage-worksite.component.html',
  styleUrls: ['./manage-worksite.component.scss']
})
export class ManageWorksiteComponent implements OnInit {
  file: File | null = null
  dataSource2: MatTableDataSource<Worksite>;
  
  worksite: Worksite[]=[];
  
  columns2: string[] = ['idWorksite', 'serialNumber','token', 'type', 'city', 'provincia','via','civico','date','actions'];
  
  @ViewChild(MatSort, { static: true }) sort2: MatSort = new MatSort;
  
  @ViewChild(MatPaginator, { static: true }) paginator2!: MatPaginator;

  constructor() {
    this.worksite=[{
      idWorksite: '1',
  serialNumber: '2324235',
  token: 'u342u3rw',
  type: 'condominio',
  city:'Empoli',
  provincia: 'Firenze',
  via: 'Via di Vittorio',
  civico: '58',
  date:'03/05/2022',
    },
    {
      idWorksite: '2',
  serialNumber: '52324235',
  token: 'u342u3rw',
  type: 'chiesa',
  city:'Montelupo',
  provincia: 'Firenze',
  via: 'Via di Vittorio',
  civico: '354',
  date:'03/05/2022',
    },
    {
      idWorksite: '3',
  serialNumber: '7892324235',
  token: 'u342u3rw',
  type: 'condominio',
  city:'Lucca',
  provincia: 'Lucca',
  via: 'Via di Vittorio',
  civico: '7',
  date:'03/05/2022',
    },
  ]

    
    this.dataSource2 = new MatTableDataSource(this.worksite);
   }

  ngOnInit(): void {
    this.dataSource2.sort = this.sort2;
    
    this.dataSource2.paginator = this.paginator2;
  }
  applyFilter2(event: any){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource2.filter = filterValue.trim().toLowerCase()
  }
  onFileInput(files: FileList | null): void {
    if (files) {
      this.file = files.item(0)
    }
  }

}
