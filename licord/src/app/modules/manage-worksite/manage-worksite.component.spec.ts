import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageWorksiteComponent } from './manage-worksite.component';

describe('ManageWorksiteComponent', () => {
  let component: ManageWorksiteComponent;
  let fixture: ComponentFixture<ManageWorksiteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageWorksiteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageWorksiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
