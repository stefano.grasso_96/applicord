import { Component, OnInit } from '@angular/core';

interface cantiere{
  tipo:string;
  citta:string;
  provincia:string;
  civico:string;
  nome: string;
  numeroLicord: number;
  lat: number;
  lng: number;
  cliente: string;
}
interface licord{
  token:string;
  battery: number;
  force1: number;
  force2: number;
  force3: number;
  temperature: number;
}

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss']
})
export class ArticlesComponent implements OnInit {
  
  cantieri:cantiere[]=[
    {
      tipo: 'Condominio',
      citta:'Empoli',
      provincia:'Firenze',
      civico:'58',
      nome:"Via Pisa",
      numeroLicord: 3,
      lat: 32543634,
      lng: 325325,
      cliente:'Rossi'
    },
    {
      tipo: 'Chiesa',
      citta:'Lucca',
      provincia:'Lucca',
      civico:'56',
      nome:"Via Roma",
      numeroLicord: 3,
      lat: 32543634,
      lng: 325325,
      cliente:'Rossi'
    },
    {
      tipo: 'Condominio',
      citta:'Livorno',
      provincia:'Livorno',
      civico:'58',
      nome:"Via Ferrara",
      numeroLicord: 3,
      lat: 32543634,
      lng: 325325,
      cliente:'Rossi'
    },
  ]

  licords :licord[]=[
    {
      token:"kdjsfbg45s46f84s86g",
      battery: 6484,
      force1: 48.5,
      force2: 61,
      force3: 54.46,
      temperature: 48,
    },
    {
      token:"iusagfiusa959",
      battery: 2421,
      force1: 435346,
      force2: 6786,
      force3: 4684,
      temperature: 6,
    },
    {
      token:"wefawg654681",
      battery: 55645,
      force1: 4365,
      force2: 53,
      force3: 245345,
      temperature: 442,
    },
    
  ]

  constructor() { }

  ngOnInit(): void {
  }
  

}
